import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/infrastructure/services/users/users.service';
import { User } from 'src/app/infrastructure/models/domain/users/user';

@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
	title = 'Пользователи';
	users: User[];

	constructor(private router: Router, private usersService: UsersService) { }

	ngOnInit(): void {
		this.usersService.getUsers().subscribe(response => 
			this.users = response.data
		);
	}

	add(){
		this.router.navigateByUrl('/root/users/add');
	}

	edit(user: User){
		this.router.navigateByUrl(`/root/users/edit/${user.id}`);
	}
}
