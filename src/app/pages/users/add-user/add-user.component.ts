import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/infrastructure/services/users/users.service';
import { Role, RoleMap } from 'src/app/infrastructure/models/domain/users/role';
import { UserBlank } from 'src/app/infrastructure/models/domain/users/userBlank';

@Component({
	selector: 'app-add-user',
	templateUrl: './add-user.component.html',
	styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

	title = 'Добавление нового пользователя';
	roles = RoleMap;
	selectedRole: Role;
	userBlank: UserBlank;

	constructor(private router: Router, private usersService: UsersService) {
		this.userBlank = new UserBlank();
	}

	ngOnInit(): void {
	}

	back() {
		this.router.navigateByUrl('/root/users');
	}

	save() {
		console.log(this.userBlank);
		this.usersService.addUser(this.userBlank).subscribe(response => {
			console.log(response);
			if(response.success){
				this.back();
			}
		});
	}
}
