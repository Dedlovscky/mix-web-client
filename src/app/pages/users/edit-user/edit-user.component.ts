import { Component, OnInit } from '@angular/core';
import { RoleMap, Role } from 'src/app/infrastructure/models/domain/users/role';
import { UserBlank } from 'src/app/infrastructure/models/domain/users/userBlank';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/infrastructure/services/users/users.service';

@Component({
	selector: 'app-edit-user',
	templateUrl: './edit-user.component.html',
	styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

	title = 'Редактирование пользователя';
	roles = RoleMap;
	selectedRole: Role;
	userBlank: UserBlank;
	
	constructor(private route: ActivatedRoute, private router: Router, private usersService: UsersService) {
		this.userBlank = new UserBlank();
		
		const id: string = route.snapshot.params.userId;

		this.usersService.getUser(id).subscribe(response => {
			console.log(response);
			if(response.success){
				const user = response.data;
				this.userBlank = new UserBlank(user.id, user.employeeId, user.userGroupIds,
					 user.username, user.role, user.isBlocked);
			}
		});
	 }

	ngOnInit(): void {
	}
	
	back() {
		this.router.navigateByUrl('/root/users');
	}

	save() {
		console.log(this.userBlank);
		this.usersService.editUser(this.userBlank).subscribe(response => {
			console.log(response);
			if(response.success){
				this.back();
			}
		});
	}
}
