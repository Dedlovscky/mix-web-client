import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/infrastructure/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  username = '';
  password = '';
  maxUsernameLength = 5;
  maxPasswordLength = 36;

  constructor(private router: Router, private authService: AuthService) { }

  signIn() {
    this.authService.signIn(this.username, this.password)
    .subscribe((response) => {
      if(response.success){
        this.router.navigateByUrl('/root');
      }
    });
  }

  ngOnInit(): void {
  }
}
