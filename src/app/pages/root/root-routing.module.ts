import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { UsersComponent } from '../users/users.component';
import { EmployeesComponent } from '../employees/employees.component';
import { RootComponent } from './root.component';
import { AddUserComponent } from '../users/add-user/add-user.component';
import { EditUserComponent } from '../users/edit-user/edit-user.component';
import { AddEmployeeComponent } from '../employees/add-employee/add-employee.component';
import { EditEmployeeComponent } from '../employees/edit-employee/edit-employee.component';

const employeeRoutes: Routes = [
  { path: 'add', component: AddEmployeeComponent },
  { path: 'edit', component: EditEmployeeComponent }
];

const userRoutes: Routes = [
  { path: 'add', component: AddUserComponent },
  { path: 'edit', component: EditUserComponent }
];

const itemRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'users', component: UsersComponent },
  { path: 'users/add', component: AddUserComponent },
  { path: 'users/edit/:userId', component: EditUserComponent },
  { path: 'employees', component: EmployeesComponent },
  { path: 'employees/add', component: AddEmployeeComponent },
  { path: 'employees/edit/:employeeId', component: EditEmployeeComponent }
];

const routes: Routes = [
  { path: '', redirectTo: '/root/home', pathMatch: 'full' },
  { path: 'root', redirectTo: '/root/home', pathMatch: 'full' },
  { path: 'root', component: RootComponent, children: itemRoutes }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RootRoutingModule { }
