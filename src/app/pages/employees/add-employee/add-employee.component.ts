import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeesService } from 'src/app/infrastructure/services/employees/employees.service';
import { Role } from 'src/app/infrastructure/models/domain/users/role';
import { EmployeeBlank } from 'src/app/infrastructure/models/domain/employees/employeeBlank';

@Component({
	selector: 'app-add-employee',
	templateUrl: './add-employee.component.html',
	styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

	title = 'Добавление нового сотрудника';
	employeeBlank: EmployeeBlank;

	constructor(private router: Router, private employeesService: EmployeesService) {
		this.employeeBlank = new EmployeeBlank();
	}

	ngOnInit(): void {

	}

	back() {
		this.router.navigateByUrl('/root/employees');
	}

	save() {
		console.log(this.employeeBlank);
		this.employeesService.addEmployee(this.employeeBlank).subscribe(response => {
			console.log(response);
			if(response.success){
				this.back();
			}
		});
	}
}
