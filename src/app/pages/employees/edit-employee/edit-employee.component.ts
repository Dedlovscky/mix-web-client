import { Component, OnInit } from '@angular/core';
import { EmployeeBlank } from 'src/app/infrastructure/models/domain/employees/employeeBlank';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeesService } from 'src/app/infrastructure/services/employees/employees.service';

@Component({
	selector: 'app-edit-employee',
	templateUrl: './edit-employee.component.html',
	styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {

	title = 'Редактирование сотрудника';
	employeeBlank: EmployeeBlank;

	constructor(private route: ActivatedRoute, private router: Router, private employeesService: EmployeesService) {
		this.employeeBlank = new EmployeeBlank();
		
		const id: string = route.snapshot.params.employeeId;

		this.employeesService.getEmployee(id).subscribe(response => {
			console.log(response);
			if (response.success) {
				const employee = response.data;
				this.employeeBlank = new EmployeeBlank(employee.id, employee.userId, employee.lastName, employee.firstName,
					employee.patronymic, employee.gender);
			}
		});
	}

	ngOnInit(): void {
	}

	back() {
		this.router.navigateByUrl('/root/employees');
	}

	save() {
		console.log(this.employeeBlank);
		this.employeesService.editEmployee(this.employeeBlank).subscribe(response => {
			console.log(response);
			if(response.success){
				this.back();
			}
		});
	}
}
