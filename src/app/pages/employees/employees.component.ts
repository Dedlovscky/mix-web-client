import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeesService } from 'src/app/infrastructure/services/employees/employees.service';
import { Employee } from 'src/app/infrastructure/models/domain/employees/employee';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

	title = 'Сотрудники';
	employees: Employee[];

	constructor(private router: Router, private employeesService: EmployeesService) { }

	ngOnInit(): void {
		this.employeesService.getEmployees().subscribe(response => 
			this.employees = response.data
		);
	}

	add(){
		this.router.navigateByUrl('/root/employees/add');
	}

	edit(employee: Employee){
		this.router.navigateByUrl(`/root/employees/edit/${employee.id}`);
	}
}
