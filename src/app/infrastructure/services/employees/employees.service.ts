import { Injectable } from '@angular/core';
import { HttpService } from '../common/http.service';
import { Observable, of } from 'rxjs';
import { ResponseAPI } from '../../models/domain/common/responseApi';
import { Employee } from '../../models/domain/employees/employee';
import { Gender } from '../../models/domain/employees/gender';
import { EmployeeBlank } from '../../models/domain/employees/employeeBlank';

@Injectable({
	providedIn: 'root'
})
export class EmployeesService {

	private employees: Employee[] = [
		new Employee('11', '1', 'Попов', 'Иван', 'Михайлович', Gender.MALE),
		new Employee('12', '2', 'Попов', 'Иван', 'Михайлович', Gender.MALE),
		new Employee('13', '3', 'Попов', 'Иван', 'Михайлович', Gender.MALE),
		new Employee('14', '4', 'Попов', 'Иван', 'Михайлович', Gender.MALE),
		new Employee('15', '5', 'Попов', 'Иван', 'Михайлович', Gender.MALE),
		new Employee('16', '6', 'Попов', 'Иван', 'Михайлович', Gender.MALE),
		new Employee('17', '7', 'Попов', 'Иван', 'Михайлович', Gender.MALE)
	]

	constructor(private httpService: HttpService) { }

	addEmployee(blank: EmployeeBlank): Observable<ResponseAPI<null>>{
		this.employees.push(new Employee('8', '8', blank.lastName, 'Иван', 'Михайлович', Gender.MALE));
		return of(ResponseAPI.successful('Сотрудник сохранен успешно'));
	}

	editEmployee(blank: EmployeeBlank): Observable<ResponseAPI<null>>{
		let employee = this.employees.find(x => x.id === blank.employeeId);

		employee.lastName = blank.lastName;

		return of(ResponseAPI.successful('Сотрудник сохранен успешно'));
	}

	getEmployee(id: string): Observable<ResponseAPI<Employee>>{
		let employee = this.employees.find(x => x.id === id);

		return of(new ResponseAPI<Employee>(true, '', employee));
	}

	getEmployees(): Observable<ResponseAPI<Employee[]>>{
		return of(new ResponseAPI<Employee[]>(true, '', this.employees));
	}
}
