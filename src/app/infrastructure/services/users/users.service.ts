import { Injectable } from '@angular/core';
import { HttpService } from '../common/http.service';
import { User } from '../../models/domain/users/user';
import { Role } from '../../models/domain/users/role';
import { Observable, of } from 'rxjs';
import { ResponseAPI } from '../../models/domain/common/responseApi';
import { UserBlank } from '../../models/domain/users/userBlank';

@Injectable({
	providedIn: 'root'
})
export class UsersService {

	private users: User[] = [
		new User('1', '11', [], 'ebole', Role.USER, false),
		new User('2', '12', [], 'ionco', Role.USER, true),
		new User('3', '13', [], 'hicom', Role.USER, false),
		new User('4', '14', [], 'cklen', Role.USER, false),
		new User('5', '15', [], 'rtula', Role.USER, true),
		new User('6', '16', [], 'adelm', Role.USER, false),
		new User('7', '17', [], 'eoute', Role.USER, true)
	];

	constructor(private httpService: HttpService) { }

	addUser(blank: UserBlank): Observable<ResponseAPI<null>>{
		this.users.push(new User('8', null, [], blank.username, blank.role, false));
		return of(ResponseAPI.successful('Пользователь сохранен успешно'));
	}

	editUser(blank: UserBlank): Observable<ResponseAPI<null>>{
		let user = this.users.find(x => x.id === blank.userId);

		user.username = blank.username;

		return of(ResponseAPI.successful('Пользователь сохранен успешно'));
	}

	getUser(id: string): Observable<ResponseAPI<User>>{
		let user = this.users.find(x => x.id === id);

		return of(new ResponseAPI<User>(true, '', user));
	}

	getUsers(): Observable<ResponseAPI<User[]>>{
		return of(new ResponseAPI<User[]>(true, '', this.users));
	}
}
