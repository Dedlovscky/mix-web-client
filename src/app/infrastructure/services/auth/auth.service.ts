import { Injectable } from '@angular/core';
import { HttpService } from '../common/http.service';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ResponseAPI } from '../../models/domain/common/responseApi';
import { Token } from '../../models/domain/common/token';
import { LocalStorageKey } from '../../models/domain/common/localStorageKey';
import { throwError, Observable, of } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private httpService: HttpService) { }

	public signIn(username: string, password: string): Observable<ResponseAPI<Object>> {
		const url = `${environment.api_url}/auth`;

		username = username === '' ? null : username;
		password = password === '' ? null : password;

		if (username === null || password === null) {
			return throwError(ResponseAPI.failed("Не указан логин или пароль"));
		}

		return of(new ResponseAPI(true, '', null));

		return this.httpService.post(url, { username, password }).pipe(map((response: ResponseAPI<Token>) => {
			this.saveToken(response.data);
			return response;
		}));
	}

	private saveToken(token: Token) {
		localStorage.setItem(LocalStorageKey.TOKEN, JSON.stringify(token));
	}
}
