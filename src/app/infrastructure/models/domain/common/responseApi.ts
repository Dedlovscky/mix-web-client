export class ResponseAPI<T> {
    success: boolean;
    message: string;
    data: T | null;

    constructor(success: boolean, message: string, data: T | null){
        this.success = success;
        this.message = message;
        this.data = data;
    }
    
    public static successful(message: string){
        return new ResponseAPI(true, message, null); 
    }

    public static failed(message: string){
        return new ResponseAPI(false, message, null); 
    }
}
