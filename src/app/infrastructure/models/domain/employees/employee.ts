import { Gender } from './gender';

export class Employee {
    id: string;
    userId: string | null;
    lastName: string;
    firstName: string;
    patronymic: string;
    gender: Gender;

    constructor(id: string, userId: string | null, lastName: string, firstName: string, patronymic: string, gender: Gender) {
        this.id = id;
        this.userId = userId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.gender = gender;
    }
}
