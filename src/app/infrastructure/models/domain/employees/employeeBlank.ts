import { Gender } from './gender';

export class EmployeeBlank {
    employeeId: string | null;
    userId: string | null;
    lastName: string | null;
    firstName: string | null;
    patronymic: string | null;
    gender: Gender | null;

    constructor(employeeId?: string, userId?: string, lastName?: string, firstName?: string, patronymic?: string,
        gender?: Gender) {
        this.employeeId = employeeId ?? null;
        this.lastName = lastName ?? null;
        this.firstName = firstName ?? null;
        this.patronymic = patronymic ?? null;
        this.gender = gender ?? null;
    }
}
