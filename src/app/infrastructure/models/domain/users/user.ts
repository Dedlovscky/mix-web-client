import { Role } from './role';

export class User{
	id: string;
	employeeId: string | null;
	userGroupIds: string[];
	username: string;
	role: Role;
	isBlocked: boolean;

	constructor(id: string, employeeId: string | null, userGroupIds: string[], username: string, role: Role, isBlocked: boolean){
		this.id = id;
		this.employeeId = employeeId;
		this.userGroupIds = userGroupIds;
		this.username = username;
		this.role = role;
		this.isBlocked = isBlocked;
	}
}