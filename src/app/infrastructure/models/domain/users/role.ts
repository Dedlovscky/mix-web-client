export enum Role {
	ADMINISTRATOR = 0,
	USER = 1
}

export const RoleMap = new Map<number, string>([
	[Role.ADMINISTRATOR, 'Администратор'],
	[Role.USER, 'Пользователь']
]);