import { Role } from './role';

export class UserBlank{
	userId: string | null;
	employeeId: string | null;
	userGroupIds: string[];
	username: string | null;
	password: string | null;
	role: Role | null;
	isBlocked: boolean;

	constructor(userId?: string, employeeId?: string, userGroupIds?: string[], username?: string, role?: Role, isBlocked: boolean = false){
		this.userId = userId ?? null;
		this.employeeId = employeeId ?? null;
		this.userGroupIds = userGroupIds ?? [];
		this.username = username ?? null;
		this.password = null;
		this.role = role ?? null;
		this.isBlocked = isBlocked;
	}
}