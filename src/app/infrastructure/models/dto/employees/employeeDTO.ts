import { Gender } from '../../domain/employees/gender';

export class EmployeeDTO {
    employeeId: string;
    userId: string;
    lastName: string;
    firstName: string;
    patronymic: string;
    gender: Gender;

    constructor(employeeId: string, userId: string, lastName: string, firstName: string, patronymic: string, gender: Gender) {
        this.employeeId = employeeId;
        this,userId = userId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.gender =  gender;
    }
}
