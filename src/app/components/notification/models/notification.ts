import { NotificationType } from './notification-type';

export abstract class Notification{
	cssClass: string;
	message: string;
	type: NotificationType;

	constructor(cssClass: string, message: string, type: NotificationType){
		this.cssClass = cssClass;
		this.message = message;
		this.type = type;
	}
}