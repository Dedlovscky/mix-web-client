import { Notification } from './notification';
import { NotificationType } from './notification-type';

export class DangerNotification extends Notification{
	constructor(message: string){
		super('cssClass', message, NotificationType.Danger)
	}
}