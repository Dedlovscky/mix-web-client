import { Notification } from './notification';
import { NotificationType } from './notification-type';

export class WarningNotification extends Notification{
	constructor(message: string){
		super('cssClass', message, NotificationType.Warning)
	}
}