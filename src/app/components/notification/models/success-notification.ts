import { Notification } from './notification';
import { NotificationType } from './notification-type';

export class SuccessNotification extends Notification{
	constructor(message: string){
		super('cssClass', message, NotificationType.Success)
	}
}