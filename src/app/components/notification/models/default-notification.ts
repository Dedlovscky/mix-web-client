import { Notification } from './notification';
import { NotificationType } from './notification-type';

export class DefaultNotification extends Notification{
	constructor(message: string){
		super('cssClass', message, NotificationType.Default);
	}
}