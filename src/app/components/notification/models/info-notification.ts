import { Notification } from './notification';
import { NotificationType } from './notification-type';

export class InfoNotification extends Notification{
	constructor(message: string){
		super('cssClass', message, NotificationType.Info)
	}
}