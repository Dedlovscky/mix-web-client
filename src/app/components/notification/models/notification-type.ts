export enum NotificationType{
	Default = 0,
	Success = 1,
	Info = 2,
	Warning = 3,
	Danger = 4
}