import { Component, OnInit } from '@angular/core';
import { Notification } from './models/notification';
import { NotificationService } from './notification.service';

@Component({
	selector: 'app-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

	notifications: Notification[] = [];

	constructor(private notificationService: NotificationService) { }

	ngOnInit(): void {
		this.notifications.push(this.notificationService.default('yyyeeess!!!'));
	}
}
