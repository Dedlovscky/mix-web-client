import { Injectable } from '@angular/core';
import { Notification } from './models/notification';
import { DefaultNotification } from './models/default-notification';
import { SuccessNotification } from './models/success-notification';
import { InfoNotification } from './models/info-notification';
import { WarningNotification } from './models/warning-notification';
import { DangerNotification } from './models/danger-notification';

@Injectable({
	providedIn: 'root'
})
export class NotificationService {

	constructor() { }

	default(message: string): Notification{
		return new DefaultNotification(message);
	}

	success(message: string): Notification{
		return new SuccessNotification(message);
	}

	info(message: string): Notification{
		return new InfoNotification(message);
	}

	warning(message: string): Notification{
		return new WarningNotification(message);
	}

	danger(message: string): Notification{
		return new DangerNotification(message);
	}
}
