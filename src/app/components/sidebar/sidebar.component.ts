import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SidebarService } from './sidebar.service';
import { MenuItem } from './models/menuItem';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers: [SidebarService]
})
export class SidebarComponent implements OnInit {

  menuItems: MenuItem[];

  constructor(private router: Router, private sidebarService: SidebarService) { }

  ngOnInit(): void {
    this.sidebarService.getMenuItems()
    .subscribe(response => 
      this.menuItems = response.data
    );
  }

  selectedMenuItem(selectedMenuItem) {
    selectedMenuItem.cssClass = '_active-item';

    const menuItems = this.menuItems.filter(x => x.id !== selectedMenuItem.id);

    for (const item of menuItems) {
      item.cssClass = '';
    }

    this.router.navigateByUrl(selectedMenuItem.url);
  }

  logout(){
    this.router.navigateByUrl('/login');
  }
}
