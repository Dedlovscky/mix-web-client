import { Injectable } from '@angular/core';
import { MenuItem } from './models/menuItem';
import { Observable, from, of } from 'rxjs';
import { ResponseAPI } from 'src/app/infrastructure/models/domain/common/responseApi';

@Injectable()
export class SidebarService {
  
  private menuItems: MenuItem[] = [
    new MenuItem('1', 'Домашняя', '_active-item', 'fas fa-home', '/root/home'),
    new MenuItem('2', 'Пользователи', '', 'fas fa-users', '/root/users'),
    new MenuItem('3', 'Сотрудники', '', 'fas fa-address-card', '/root/employees')
  ];

  constructor() { }

  getMenuItems(): Observable<ResponseAPI<MenuItem[]>>{
    return of(new ResponseAPI<MenuItem[]>(true, '', this.menuItems));
  }
}
