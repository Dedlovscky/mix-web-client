export class MenuItem{
    id: string;
    title: string;
    cssClass: string;
    icon: string;
    url: string;

    constructor(id: string, title: string, cssClass: string, icon: string, url: string){
        this.id = id;
        this.title = title;
        this.cssClass = cssClass;
        this.icon = icon;
        this.url = url;
    }
}