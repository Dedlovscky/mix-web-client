import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RootModule } from './pages/root/root.module';
import { RootComponent } from './pages/root/root.component';

const routes: Routes = [
  { path: '', redirectTo: '/root', pathMatch: 'full' },
  { path: 'root', component: RootComponent},
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [
    RootModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
